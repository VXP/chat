SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `chat`;
USE `chat`;

CREATE TABLE `message` (
  `idMessage` int NOT NULL,
  `text` varchar(1024) DEFAULT NULL,
  `date` timestamp DEFAULT current_timestamp,
  `User_idUser` int NOT NULL,
  `room_idRoom` int NOT NULL
);

CREATE TABLE `room` (
  `idRoom` int NOT NULL,
  `name` varchar(45) DEFAULT NULL
);

CREATE TABLE `user` (
  `idUser` int NOT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(65) DEFAULT NULL,
  `sessionid` varchar(40) DEFAULT NULL,
  `accesslevel` int NOT NULL DEFAULT '0'
);


ALTER TABLE `message`
  ADD PRIMARY KEY (`idMessage`,`user_idUser`,`room_idRoom`),
  ADD KEY `fk_message_user_idx` (`user_idUser`),
  ADD KEY `fk_message_room1_idx` (`room_idRoom`);

ALTER TABLE `room`
  ADD PRIMARY KEY (`idRoom`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);


ALTER TABLE `message`
  MODIFY `idMessage` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

ALTER TABLE `room`
  MODIFY `idRoom` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

ALTER TABLE `user`
  MODIFY `idUser` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;


ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_room1` FOREIGN KEY (`room_idRoom`) REFERENCES `room` (`idRoom`),
  ADD CONSTRAINT `fk_message_user` FOREIGN KEY (`user_idUser`) REFERENCES `user` (`idUser`);



INSERT INTO `room` (`idRoom`, `name`) VALUES
(0, 'Guest Room');


COMMIT;
