<?php
// if ( isset( $_COOKIE["PHPSESSID"] ) )
// {
	// header( "Location: index.php" );
// }

include_once( $_SERVER['DOCUMENT_ROOT'] . "/api/shared.php" );

$dummy = -1;
if ( userAccess( $dummy ) )
{
	header( "Location: index.php" );
}
?>
<html>
<head>
<meta name="viewport" content="width=400">
<title>Регистрация</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script>
function getXmlHttp(){var xmlhttp;
try{xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){
try{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}catch(E){xmlhttp=false;}}
if(!xmlhttp && typeof XMLHttpRequest!='undefined'){xmlhttp=new XMLHttpRequest();}return xmlhttp;}

function register()
{
	var xmlhttp = getXmlHttp();

	var login = document.getElementById( "login" ).value;
	var password = document.getElementById( "password" ).value;
	var password2 = document.getElementById( "password2" ).value;

	if ( password.localeCompare( password2 ) !== 0 )
	{
		showError( "Пароль и его подтверждение не совпадают" )
		return;
	}

	xmlhttp.open( 'POST', 'api/auth.php', true );
	xmlhttp.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
	xmlhttp.send( "login=" + encodeURIComponent( login ) + "&password=" + encodeURIComponent( password ) + "&password2=" + encodeURIComponent( password2 ) );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				console.log( xmlhttp.responseText );
				if ( xmlhttp.responseText.localeCompare( "" ) == 0 )
				{
					console.log( "Регистрация завершена" );
					window.location = "login.php";
				}
				else
				{
					console.log( "Регистрация не завершена" );
					showError( xmlhttp.responseText )
				}
			}
		}
	};

}

function showError( message )
{
	if ( message === undefined || message === "" )
	{
		document.getElementById( "error" ).style.visibility = "hidden";
		return;
	}

	document.getElementById( "error" ).style.visibility = "visible";
	document.getElementById( "error" ).innerHTML = message;
}
</script>
</head>
<body>
	Зарегистрируйтесь в чате, введя желаемый логин и пароль:<br>
	Если Вы уже зарегистрированы - <a href="login.php">войдите в чат</a> с помощью Вашего логина и пароля<br>
	<br>
	<input type="text" id="login" name="login" value="" placeholder="Логин"><br>
	<input type="password" id="password" name="password" value="" placeholder="Пароль"><br>
	<input type="password" id="password2" name="password2" value="" placeholder="Подтверждение пароля"><br>
	<button onclick="register();">Зарегистрироваться</button>
	<div id="error"></div>
</body>
</html>