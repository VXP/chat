<?php
// API
// /api/?mode=getrooms
// /api/?mode=getchat&room=id
// /api/?mode=sendmessage&room=id&message=text

// Проверка, если пользователь не зашёл (по кукам) - предложить логин или регистрацию

include_once( $_SERVER['DOCUMENT_ROOT'] . "/api/shared.php" );

$dummy = -1;
if ( !userAccess( $dummy ) )
{
//	echo "REDIRECT TO LOGIN: " . $test;
	header( "Location: login.php" );
}

// $sessionID = -1;
// echo getsessionid( $sessionID );
// echo $sessionID;

$retaccess = -1;
userAccess( $retaccess );
$g_greaterOrEqualAsModerator = ($retaccess >= ACCESS::MODERATOR);

?>

<html>
<head>
<meta name="viewport" content="width=400">
<title>Chat</title>
<?php
echo '<link rel="stylesheet" type="text/css" href="style.css?' . filemtime($_SERVER["DOCUMENT_ROOT"] . '/style.css') . '">';
echo "\n";
?>
<script>
function getXmlHttp(){var xmlhttp;
try{xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){
try{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}catch(E){xmlhttp=false;}}
if(!xmlhttp && typeof XMLHttpRequest!='undefined'){xmlhttp=new XMLHttpRequest();}return xmlhttp;}

// alert(navigator.userAgent);
// alert(window.devicePixelRatio);

function generateSmiles()
{
	var smiles = document.getElementById( "smiles" );
	var btnOpenSmiles = document.getElementById( "btnOpenSmiles" );
//	smiles.innerHTML = String.fromCharCode(0xD83D, 0xDE04);
//	smiles.innerHTML = String.fromCharCode(0xD83D);
//	for ( var i = 0x1F604, j = 0; i < 0x1F64F; i++, j++ )
	for ( var i = 0x1F604; i < 0x1F64F; i++ )
	{
	//	smiles.innerHTML += String.fromCodePoint( i );
	//	smiles.innerHTML += "<span id=\"smile" + j + "\" onclick=\"typeSmile( " + i + " );\">" + String.fromCodePoint( i ) + "</span>";
		smiles.innerHTML += "<span id=\"smile" + i
			+ "\" onclick=\"typeSmile( " + i + " );\""
			+ ">"
			+ String.fromCodePoint( i ) + "</span>";
	}

	btnOpenSmiles.innerText = String.fromCodePoint( 0x1F604 )
}

function typeText( text, focusOnTextEntryInput = false )
{
	var textentryinput = document.getElementById( "textentryinput" );
	textentryinput.value += text;
	if ( focusOnTextEntryInput )
		textentryinput.focus();
	textentryinput.setSelectionRange( textentryinput.value.length, textentryinput.value.length );
}

function typeSmile( smilehex, focusOnTextEntryInput = false )
{
	var textentryinput = document.getElementById( "textentryinput" );
	textentryinput.value += String.fromCodePoint( smilehex );
	if ( focusOnTextEntryInput )
		textentryinput.focus();
	textentryinput.setSelectionRange( textentryinput.value.length, textentryinput.value.length );
}

var g_roomid = -1;
var g_lastmessageid = -1;

let eventSource
function startEventSource()
{
	if ( eventSource !== null && eventSource !== undefined )
		eventSource.close();
	
	if ( g_roomid === null || g_roomid === undefined )
		return;
	if ( g_lastmessageid === null || g_lastmessageid === undefined )
		return;

	eventSource = new EventSource( "api/events.php?roomid=" + g_roomid + "&lastmessageid=" + g_lastmessageid );
	eventSource.onopen = function( e ) {
		console.log( "onopen" );
		console.log( e );
	};
	eventSource.onerror = function( e ) {
		console.log( "onerror:" );
		console.log( e );
		if (this.readyState == EventSource.CONNECTING)
		{
			console.log(`Переподключение (readyState=${this.readyState})...`);
		}
		else
		{
			console.log("Произошла ошибка.");
		}
	};
	eventSource.addEventListener( "ping", function( e ) {
		console.log( "ping: " + e.data );
	});
	eventSource.addEventListener( "newmessage", function( e ) {
		console.log( "newmessage:" );
		console.log( e.data );
		addMessageToListAndScroll( JSON.parse( e.data ) );
	});
	eventSource.onmessage = function( e ) {
		console.log( "onmessage: " + e.data );
	};
}

function getrooms()
{
	var xmlhttp = getXmlHttp();
	xmlhttp.open( "GET", "api/index.php?mode=getrooms", true );
	xmlhttp.send( null );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				if ( xmlhttp.responseText === null || xmlhttp.responseText === undefined )
					return;

				var obj
				try
				{
					obj = JSON.parse( xmlhttp.responseText );
				}
				catch( e )
				{
				//	return;
				}

				if ( obj === null || obj === undefined )
					return;

				console.log(obj);
				g_roomid = parseInt( obj[0].idRoom ); // По-умолчанию первая комната
			//	document.getElementById( "roomname" ).innerHTML = obj[0].name;

			//	document.getElementById( "roomname" ).innerHTML = "<select>\n";
				document.getElementById( "roomnameselect" ).innerHTML = ""; // Clear the list
				for ( i = 0; i < obj.length; i++ )
				{
					document.getElementById( "roomnameselect" ).innerHTML += "<option value=\"" + obj[i].idRoom + "\">" + obj[i].name + "</option>\n";
				}
			//	document.getElementById( "roomname" ).innerHTML += "</select>\n";

			//	getchat( roomid );
				getchat();
			}
		}
	};
}

var contextmenu
function createContextMenu()
{
	contextmenu = document.createElement( "div" );
	contextmenu.className = "contextmenu";
//	contextmenu.innerHTML = "HELLO THERE SUGARTITS";
	contextmenu.style.position = "absolute";
	contextmenu.style.width = "150px";
	contextmenu.style.height = "150px";
	contextmenu.style.backgroundColor = "rgba(100, 100, 100, 100)";

	window.addEventListener( "click", function( e ) {
		contextmenu.style.visibility = "hidden";
	} );
}

function populateContextMenu( target )
{
	contextmenu.innerHTML = "";

	const btnCtxMenuRemoveMessage = document.createElement( "button" );
	btnCtxMenuRemoveMessage.innerText = "Remove message #" + target.id;
	btnCtxMenuRemoveMessage.addEventListener( "click", function() {
		removemessage( target );
	}, false );

	const btnCtxMenuMentionUser = document.createElement( "button" );
	btnCtxMenuMentionUser.innerText = "Mention";
	btnCtxMenuMentionUser.addEventListener( "click", function() {
		typeText( "@" + target.chatUserName + " ", true );
	}, false );

	contextmenu.appendChild( btnCtxMenuRemoveMessage );
	contextmenu.appendChild( btnCtxMenuMentionUser );
}


function openContextMenu( target, x, y )
{
	if ( contextmenu === null || contextmenu === undefined )
	{
		createContextMenu();
	}

	// If pressed right-click on a contextmenu itself
	if ( target == contextmenu )
		return;

	populateContextMenu( target );

	contextmenu.style.left = x + "px";
	contextmenu.style.top = y + "px";
	contextmenu.style.visibility = "visible";
	target.appendChild( contextmenu );
}

function addMessageToList( obj )
{
	for ( i = 0; i < obj.length; i++ )
	{
	//	var login = "Guest";
	//	var login = obj[i].login;
	/*
		document.getElementById( "chatbox" ).innerHTML += 
			"[" + 
			obj[i].date + 
			"] " + 
			login + 
			": " + 
			obj[i].text + 
			"<br>\n";
	*/
		var comingMessage = obj[i];
		
		if ( comingMessage === null || comingMessage === undefined )
			continue;
		console.log(comingMessage);

		var idMessage = comingMessage.idMessage;
		console.log( "Adding ID#" + idMessage );
		
		// <div class="message" id="94" title="2024-07-23 22:54:00"><input type="checkbox" onclick="handlecheckbox( this );">UAVXP: rtherhre</div>

	//	document.getElementById( "chatbox" ).innerHTML += 
	//		"<div class='message' id='" + idMessage + "' title='" + obj[i].date + "'>" +

<?php
//if ( $g_greaterOrEqualAsModerator ) // TODO: Хак!
//{
//	echo "\t\t\t// Admin-only\n";
//	echo "\t\t\t\"<input type='checkbox' onclick='handlecheckbox( this );'/>\" +\n";
//}
?>

	//		login + 
	//		": " + 
	//		obj[i].text + 
	//		"</div>\n";
	
		var messageInner = "";

<?php
if ( $g_greaterOrEqualAsModerator ) // TODO: Хак!
{
//	echo "\t\t\t// Admin-only\n";
	echo "\t\tmessageInner += \"<input type='checkbox' onclick='handlecheckbox( this );'/>\";\n";
}
?>

		messageInner += comingMessage.login + ": " + comingMessage.text;
	
		const message = document.createElement( "div" );
		message.className = "message";
		message.id = idMessage;
		message.chatUserName = comingMessage.login;
		message.title = comingMessage.date;
		message.innerHTML = messageInner;
		message.addEventListener( "contextmenu", function( e )
		{
			// target == currentTarget
			console.log(e)
			console.log( "target", e.target );
			console.log( "currentTarget", e.currentTarget );
			console.log( "relatedTarget", e.relatedTarget );
			// clientX/Y
			// layerX/Y
			// movementX/Y
			// offsetX/Y
			// pageX/Y
			// screenX/Y
			// x/y

		//	var messageObject = e.target;
		//	removemessage( messageObject );

			openContextMenu( e.target, e.pageX, e.pageY );

			e.preventDefault();
		}, false );
		document.getElementById( "chatbox" ).appendChild( message );
		
	}
	var lastobject = obj[obj.length - 1];
	if ( lastobject != undefined )
	{
		g_lastmessageid = obj[obj.length - 1].idMessage;
		startEventSource();
	}
}

//function getchat( roomid )
function getchat()
{
	var xmlhttp = getXmlHttp();
	xmlhttp.open( "GET", "api/index.php?mode=getchat&room=" + g_roomid, true );
	xmlhttp.send( null );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				if ( xmlhttp.responseText === null || xmlhttp.responseText === undefined )
					return;

				var obj
				try
				{
					obj = JSON.parse( xmlhttp.responseText );
				}
				catch( e )
				{
				//	return;
				}
				
				if ( obj === null || obj === undefined )
					return;

				document.getElementById( "chatbox" ).innerHTML = "";

			//	console.log( obj );

				// Shared
				addMessageToList( obj );
				// Shared end

				console.log( "Starting: last message ID is " + g_lastmessageid );
				
				window.scrollTo( 0, document.body.scrollHeight );
				
				// Start receiving SSE
				startEventSource();
			}
		}
	};
}

function addMessageToListAndScroll( obj )
{
	// Shared
	addMessageToList( obj );
	// Shared end

	// Scroll down
//	console.log( window.innerHeight + document.body.scrollTop, document.body.offsetHeight );
	if ( (window.innerHeight + document.body.scrollTop + 50) >= document.body.offsetHeight )
	{
		document.getElementById( "messageNotify" ).style.visibility = "hidden";
	}
	
	// TODO: Scroll up to load previous messages and append them above
	
	if ( obj.length > 0 ) // Не обновлять, если ничего не пришло
	{
		if ( (window.innerHeight + document.body.scrollTop + 50) >= document.body.offsetHeight )
		{
			window.scrollTo( 0, document.body.scrollHeight );
			document.getElementById( "messageNotify" ).style.visibility = "hidden";
		}
		else
		{
			// Уведомить пользователя о новых сообщениях внизу чата
			document.getElementById( "messageNotify" ).style.visibility = "visible";
		}
	}
}

function getlastmessages( lastmessageid )
{
	var xmlhttp = getXmlHttp();
	xmlhttp.open( "GET", "api/index.php?mode=getlastmessages&room=" + g_roomid + "&messageid=" + lastmessageid, true );
	xmlhttp.send( null );
	xmlhttp.onreadystatechange = function()
	{
	//	console.log( xmlhttp.status );
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 422 )
			{
				// Nothing changed
				return;
			}

			if ( xmlhttp.status == 200 )
			{
				showError(); // Убираем плашку с ошибкой, поскольку от сервера что-то всё-таки пришло

				if ( xmlhttp.responseText === null || xmlhttp.responseText === undefined )
					return;

				var obj
				try
				{
					obj = JSON.parse( xmlhttp.responseText );
				}
				catch( e )
				{
				//	return;
				}
				
				if ( obj === null || obj === undefined )
					return;
				
			//	console.log( "Getting last " + obj.length + " messages from message ID " + lastmessageid );
			//	console.log( obj );

				addMessageToListAndScroll( obj );
			}
			else // Если сеть недоступна, или сервер ушёл в оффлайн
			{
				// Показать плашку с ошибкой
				showError( "Возможно, возникли некоторые неполадки с соединением или сервером. Соединение возобновится в ближайшее время." );
			}
		}
	};
}

function showError( message )
{
	if ( message === undefined || message === "" )
	{
		document.getElementById( "error" ).style.visibility = "hidden";
		return;
	}

	document.getElementById( "error" ).style.visibility = "visible";
	document.getElementById( "error" ).innerHTML = message;
}

function selectroom( room )
{
	g_roomid = room;
	getchat();
}

function sendmessage( bFromTextInput )
{
	if ( bFromTextInput && event.keyCode != 13 )
		return;

	var message = document.getElementById( "textentryinput" ).value.trim();
	document.getElementById( "textentryinput" ).value = "";
	if ( message === "" )
		return;

	var xmlhttp = getXmlHttp();
	xmlhttp.open( "GET", "api/index.php?mode=sendmessage&room=" + g_roomid + "&message=" + encodeURIComponent( message ), true );
	xmlhttp.send( null );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				if ( xmlhttp.responseText === null || xmlhttp.responseText === undefined )
					return;

			//	console.log( xmlhttp.responseText );
			//	getchat(); // Нужно другой способ обновления чата

				console.log( "Last message ID: " + g_lastmessageid );
				getlastmessages( g_lastmessageid );

				var tempmessageid = parseInt( xmlhttp.responseText );
				if ( tempmessageid != -1 )
				{
					g_lastmessageid = tempmessageid;
					toggleSmiles( false ); // Закрываем панель со смайлами
				//	document.getElementById( "textentryinput" ).focus();
				}
				else
				{
					alert( "Сообщение не было отправлено" );
				}
			}
		}
	};
}

function getnickname()
{
	var xmlhttp = getXmlHttp();
	xmlhttp.open( "GET", "api/index.php?mode=getnickname", true );
	xmlhttp.send( null );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				if ( xmlhttp.responseText === null || xmlhttp.responseText === undefined )
					return;

				document.getElementById( "nickname" ).innerHTML = "Здравствуйте, " + xmlhttp.responseText + ".";
			}
		}
	};
}

function toggleSmiles( override )
{
	var smiles = document.getElementById( "smiles" );
	if ( override !== undefined )
	{
		smiles.style.visibility = override && "visible" || "hidden";
		return;
	}

	if ( smiles.style.visibility == "visible" )
		smiles.style.visibility = "hidden";
	else
		smiles.style.visibility = "visible";
}

function createcontextmenu()
{
	if ( document.addEventListener )
	{
		document.addEventListener( "contextmenu", function( e )
		{
		//	console.log( "target", e.target );
		//	console.log( "currentTarget", e.currentTarget );
		//	console.log( "relatedTarget", e.relatedTarget );

			if ( e.button !== 2 )
				return;

			var messageid = e.target.id;
		//	removemessage( messageid );

			e.preventDefault();
		}, false );
	}
}

function removemessage( messageObject )
{
	var messageids = [];
	messageids.push( messageObject.id );

	var xmlhttp = getXmlHttp(); 
	console.log(JSON.stringify( messageids ));
	xmlhttp.open( "GET", "api/index.php?mode=removemessage&roomid=" + g_roomid + "&messages=" + JSON.stringify( messageids ), true );
	xmlhttp.setRequestHeader( "Content-Type", "application/json" );
	xmlhttp.send( null );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				if ( xmlhttp.responseText === null || xmlhttp.responseText === undefined )
					return;

				if ( xmlhttp.responseText != true )
					return;

			//	alert( xmlhttp.responseText );
			//	getchat();

				// TODO: Брать не весь чат, а сообщения с самого первого выбранного чекбоксами ID. Так же очищать все сообщения после него
				// Вариант получше - сразу просто удалять объект из DOM - лучше, чем делать ещё один запрос серверу
			//	document.getElementById( id ).remove();
				messageObject.remove();

			//	getlastmessages( messageids[0] );
			}
		}
	};
}

function removemessages()
{
	var checkboxes = document.querySelectorAll( "#chatbox input[type='checkbox']:checked" );
//	console.log( checkboxes[0].parentNode.id );

	var messageids = [];
	checkboxes.forEach( function( item, i, arr )
	{
		messageids.push( item.parentNode.id );
	} );

	var xmlhttp = getXmlHttp();
	console.log(JSON.stringify( messageids ));
	xmlhttp.open( "GET", "api/index.php?mode=removemessage&roomid=" + g_roomid + "&messages=" + JSON.stringify( messageids ), true );
	xmlhttp.setRequestHeader( "Content-Type", "application/json" );
	xmlhttp.send( null );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				if ( xmlhttp.responseText === null || xmlhttp.responseText === undefined )
					return;

				if ( xmlhttp.responseText != true )
					return;

			//	alert( xmlhttp.responseText );
			//	getchat();
				// TODO: Брать не весь чат, а сообщения с самого первого выбранного чекбоксами ID. Так же очищать все сообщения после него
				// Вариант получше - сразу просто удалять объект из DOM - лучше, чем делать ещё один запрос серверу
				for ( var i = 0; i < checkboxes.length; i++ )
				{
					checkboxes[i].parentNode.remove();
				//	delete checkboxes[i];
				}
				delete checkboxes;
			//	getlastmessages( messageids[0] );
			}
		}
	};
}

function handlecheckbox( checkbox )
{
	var btnDelete = document.getElementById( "btnDelete" );

	var message = checkbox.parentNode;
	var checkboxes = document.querySelectorAll( "#chatbox input[type='checkbox']:checked" );
	if ( checkboxes === undefined || checkboxes === null || checkboxes.length <= 0 )
	{
		// Убрать все кнопки
		btnDelete.style.visibility = "hidden";
		return;
	}

	if ( checkboxes.length == 1 )
	{
		// Показать кнопку "Редактировать сообщение"
	}
	else
	{
		// Убрать кнопку "Редактировать сообщение"
	}

	// Показать кнопку "Удалить сообщения"
	btnDelete.style.visibility = "visible";

/*
	for ( var i = 0; i < checkboxes.length; i++ )
	{
		var check = checkboxes[i];
	//	if ( check === checkbox )

		
	}
*/
}

function togglecheckboxes( obj )
{
	var checkboxes = document.querySelectorAll( "#chatbox input[type='checkbox']" );
	checkboxes.forEach( function( item, i, arr )
	{
		item.checked = obj.checked;
	} );

	handlecheckbox( checkboxes[0] );
}

function addownroom()
{
	var newRoomName = prompt( "Название будущей комнаты:", "" );

	if ( newRoomName === "" )
		return;

	var xmlhttp = getXmlHttp();
	xmlhttp.open( "GET", "api/index.php?mode=addownroom&roomname=" + newRoomName, true );
	xmlhttp.send( null );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				if ( xmlhttp.responseText === null || xmlhttp.responseText === undefined )
					return;

				var newroomid = parseInt( xmlhttp.responseText );
				if ( newroomid != -1 )
				{
					// TODO: Immediately change room to this new one
					getrooms();
				}
				else
				{
					// TODO: Something went wrong
				}
			}
		}
	};
}

getnickname();
getrooms();
//createcontextmenu();

//setInterval( getchat, 1000 );

//setInterval( function() {
//	getlastmessages( g_lastmessageid );
//}, 1000 );


</script>
</head>
<body>
<div id="header">
	<div id="roomname">
		Комнаты: 
		<select id="roomnameselect" onchange="selectroom(this.selectedIndex);"></select>
		<button id="btnRoomAdd" onclick="addownroom();">Добавить свою</button>
	</div>
	<div id="logout">
<?php
if ( $g_greaterOrEqualAsModerator ) // TODO: Хак!
{
	echo"\t<!-- <button id=\"btnEdit\" style=\"visibility: hidden;\">Редактировать сообщение</button> -->";
	echo"\t<button id=\"btnDelete\" style=\"visibility: hidden;\" onclick=\"removemessages();\">Удалить сообщения</button>";
	echo"\t<input type=\"checkbox\" onclick=\"togglecheckboxes( this );\"/>";
}
?>
		<span id="nickname"></span>
		<button onclick="document.cookie='PHPSESSID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;'; window.location='.'">Выйти</button>
	</div>
	<br>
</div>
<br>
<div id="chatbox"></div>
<div id="error"></div>
<div id="smiles"></div>
<div id="textentry">
	<div id="messageNotify" onclick="window.scrollTo( 0, document.body.scrollHeight ); this.style.visibility = 'hidden'">Есть новые сообщения</div>
	<div id="textentrysendsection">
		<input id="textentryinput" type="text" maxlength="256" size="100" onkeydown="sendmessage( true );">
		<!--
		<a href="https://unicode-table.com/ru/blocks/emoticons/" target="_blank">Смайлы</a>
		<br>

		<div id="smiles"></div>
	-->
		<button id="btnOpenSmiles" onclick="toggleSmiles();">і</button>
		<button id="btnSendMessage" onclick="sendmessage( false );">Отправить</button>
	</div>
</div>

<script>
generateSmiles();
</script>

</body>
</html>