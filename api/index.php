<?php

//$ARGS = $_GET;
$ARGS = $_REQUEST;
if ( !isset( $ARGS["mode"] ) )
{
	exit( "no data" );
}
$mode = $ARGS["mode"];

include_once( $_SERVER['DOCUMENT_ROOT'] . "/api/config.php" );

class Access
{
	const BANNED = -1;
	const USER = 0;
	const MODERATOR = 1;
	const ADMIN = 2;
	const OWNER = 3;
}

function getsessionid( &$id )
{
	if ( session_start() == false )
	{
		return false;
	}

	$tmpid = session_id();
	if ( strcmp( $tmpid, "" ) == 0 )
	{
		return false;
	}

	$id = $tmpid;
	return true;
}

function getrooms()
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

	$query = "SELECT * FROM room";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	$arr = mysqli_fetch_array( $result, mysqli_ASSOC ); // Only on loops?
	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr )); 

	mysqli_free_result( $result );
	mysqli_close( $link );

	return $arr;
}

function getchat( $roomid )
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

//	$query = "SELECT * FROM Message WHERE room_idRoom=$roomid";
	$query = "SELECT * FROM ( SELECT idMessage, text, date, login FROM message, user WHERE room_idRoom=$roomid AND message.user_idUser=user.idUser ORDER BY message.idMessage DESC LIMIT 100 ) sub ORDER BY idMessage ASC";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	$arr = mysqli_fetch_array( $result, mysqli_ASSOC ); // Only on loops?
	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr ));

	mysqli_free_result( $result );
	mysqli_close( $link );

	return $arr;
}

function sendmessage( $roomid, $message )
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

	// Shared
	$sessionID = -1;
	if ( getsessionid($sessionID) == false )
	{
		return -1;
	}
	// End shared

//	echo $sessionID;

	$message = mysqli_real_escape_string( $link, $message );

//	$query = "INSERT INTO Message (text, date, User_idUser, room_idRoom) VALUES('$message', CURTIME(), -1, $roomid)"; /// TODO!!! UserID
//	$query = "INSERT INTO Message (text, date, User_idUser, room_idRoom) SELECT '$message', CURTIME(), src.idUser, $roomid FROM User AS src WHERE src.sessionid='$sessionID'";
//	$query = "INSERT INTO Message (text, date, User_idUser, room_idRoom) VALUES('$message', CURTIME(), (SELECT idUser FROM User WHERE sessionid='$sessionID'), $roomid)";
	$query = "INSERT INTO message SET text='$message', user_idUser=COALESCE((SELECT idUser FROM user WHERE sessionid='$sessionID'), -1), room_idRoom='$roomid'";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	echo $result;

	$lastmessageid = mysqli_insert_id( $link );

//	mysqli_free_result( $result ); // No mysqli_fetch_assoc - no gain
	mysqli_close( $link );

	return $lastmessageid;
}

function getmessagesrange( $roomid, $beginmessageid, $range )
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid AND Message.User_idUser=User.idUser LIMIT 5000 OFFSET $beginmessageid";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid LIMIT $beginmessageid, 5000";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid LIMIT 5000 OFFSET $beginmessageid";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid AND Message.User_idUser=User.idUser AND Message.idMessage>=$beginmessageid";
	$query = "SELECT idMessage, text, date, login FROM message, user WHERE room_idRoom=$roomid AND message.user_idUser=user.idUser AND message.idMessage>$beginmessageid ORDER BY message.idMessage LIMIT $range";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	$arr = mysqli_fetch_array( $result, mysqli_ASSOC ); // Only on loops?
	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr ));

	mysqli_free_result( $result );
	mysqli_close( $link );

	return $arr;
}

function getlastmessages( $roomid, $lastmessageid )
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );
	
	// $messagecount = mysqli_query( $link, "SELECT COUNT(*) FROM message" ) or die( 'Cannot query: ' . mysqli_error( $link ) );
	// while(($arr0[] = mysqli_fetch_assoc( $messagecount )) || array_pop( $arr0 ));
	// echo ( $arr0[1] );

//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid AND Message.User_idUser=User.idUser LIMIT 5000 OFFSET $lastmessageid";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid LIMIT $lastmessageid, 5000";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid LIMIT 5000 OFFSET $lastmessageid";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid AND Message.User_idUser=User.idUser AND Message.idMessage>=$lastmessageid";
	$query = "SELECT idMessage, text, date, login FROM message, user WHERE room_idRoom=$roomid AND message.user_idUser=user.idUser AND message.idMessage>$lastmessageid ORDER BY message.idMessage";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	$arr = mysqli_fetch_array( $result, mysqli_ASSOC ); // Only on loops?
	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr ));

	mysqli_free_result( $result );
	mysqli_close( $link );

	return $arr;
}

function getnickname()
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

	// Shared
	$sessionID = -1;
	if ( getsessionid($sessionID) == false )
	{
		return -1;
	}
	// End shared

	$query = "SELECT login FROM user WHERE sessionid='$sessionID'";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	$arr = mysqli_fetch_array( $result, mysqli_ASSOC ); // Only on loops?
	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr )); 

	mysqli_free_result( $result );
	mysqli_close( $link );

	// Проверки
	if ( count($arr) <= 0 )
	{
		return "";
	}

	$nickname = $arr[0]["login"];
	return $nickname;
}

function removemessage( $roomid, $messageids )
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	if ( count( $messageids ) <= 0 )
		return false;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

	// Shared
	$sessionID = -1;
	if ( getsessionid($sessionID) == false )
	{
		return false;
	}
	// End shared

	// Первый запрос - берём уровень доступа залогиненного пользователя
	$query = "SELECT accesslevel FROM user WHERE sessionid='$sessionID'";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr )); 
	mysqli_free_result( $result );

	// Проверки
	if ( count($arr) <= 0 )
	{
		mysqli_close( $link );
		return false;
	}

	// TODO: Use userAccess from api/shared.php
	// $retaccess = -1;
	// userAccess( $retaccess );
	// $g_greaterOrEqualAsModerator = ($retaccess >= ACCESS::MODERATOR);
	$access = $arr[0]["accesslevel"];
	if ( $access <= Access::USER )
	{
		mysqli_close( $link );
		return false;
	}

	// Второй запрос - удаляем сообщение
//	$query = "DELETE FROM Message WHERE room_idRoom='$roomid' AND idMessage='$messageid' LIMIT 1";
	if ( gettype( $messageids ) == "integer" )
	{
		$query = "DELETE FROM message WHERE room_idRoom='$roomid' AND idMessage='$messageids'";
	}
	else
	{
		$query = "DELETE FROM message WHERE room_idRoom='$roomid' AND idMessage IN (" . implode( ",", $messageids ) . ")";
	}
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

	// Проверять нечего (постоянно возвращается LIMIT, то есть, 1), поэтому верим на слово

//	mysqli_free_result( $result ); // No mysqli_fetch_assoc - no gain
	mysqli_close( $link );

	return true;
}

function addownroom( $roomname )
{
	// TODO: Add relation between room and user, 
	// so he can manage messages in his own rooms like owner
	
	// TODO: Check if a room with the same name is already exists

	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

	// Shared
	// $sessionID = -1;
	// if ( getsessionid($sessionID) == false )
	// {
		// return -1;
	// }
	// End shared

//	echo $sessionID;

	$query = "SELECT name FROM room WHERE name='$roomname'";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );
	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr )); 
	mysqli_free_result( $result );
	if ( count($arr) > 0 )
	{
		return -1;
	}

	$query = "INSERT INTO room SET name='$roomname'";
	// $query = "IF NOT EXISTS(SELECT name FROM room WHERE name='$roomname')
// BEGIN
	// INSERT INTO room SET name='$roomname'
// END";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	echo $result;

	$newroomid = mysqli_insert_id( $link );

//	mysqli_free_result( $result ); // No mysqli_fetch_assoc - no gain
	mysqli_close( $link );

	return $newroomid;
}



if ( strcmp( $mode, "getrooms" ) == 0 )
{
	// $rooms = array(
		// "Main",
		// "General"
	// );

	$rooms = getrooms();
	exit( json_encode( $rooms, JSON_UNESCAPED_UNICODE ) );
}
elseif ( strcmp( $mode, "getchat" ) == 0 )
{
	$room = $ARGS["room"];
	$chat = getchat( $room );
	exit( json_encode( $chat, JSON_UNESCAPED_UNICODE ) );
}
elseif ( strcmp( $mode, "sendmessage" ) == 0 )
{
	$room = $ARGS["room"];
	$message = $ARGS["message"];

	$message = trim( $message );
	if ( strcmp( $message, "" ) == 0 )
	{
		exit();
	}

	$lastmessageid = sendmessage( $room, $message );

	exit( json_encode( $lastmessageid, JSON_UNESCAPED_UNICODE ) ); // Возвращает индекс только что присланного сообщения
}
elseif ( strcmp( $mode, "getmessagesrange" ) == 0 )
{
	$room = $ARGS["room"];
	$beginmessageid = $ARGS["beginmessageid"];
	$range = $ARGS["range"]; // Up. Message count

	$messages = getmessagesrange( $room, $beginmessageid, $range );
	exit( json_encode( $messages, JSON_UNESCAPED_UNICODE ) );
}
elseif ( strcmp( $mode, "getlastmessages" ) == 0 )
{
	$room = $ARGS["room"];
	$lastmessageid = $ARGS["messageid"];

	$messages = getlastmessages( $room, $lastmessageid );

	if ( count($messages) <= 0 )
	{
	//	http_response_code( 422 ); // Unprocessable Content
		exit();
	}

	exit( json_encode( $messages, JSON_UNESCAPED_UNICODE ) );
}
elseif ( strcmp( $mode, "getnickname" ) == 0 )
{
	$nickname = getnickname();
	exit( $nickname );
}
elseif ( strcmp( $mode, "addroom" ) == 0 )
{
	$roomname = $ARGS["roomname"];
	addroom( $roomname );
}
elseif ( strcmp( $mode, "removemessage" ) == 0 )
{
	$roomid = $ARGS["roomid"];
	$messages = $ARGS["messages"];

	$messageids = json_decode( $messages );

	$done = removemessage( $roomid, $messageids );
	exit( $done );
}
elseif ( strcmp( $mode, "addownroom" ) == 0 )
{
	$roomname = $ARGS["roomname"];

	if ( strcmp( $roomname, "" ) == 0 )
	{
		exit();
	}

	$newroomid = addownroom( $roomname );
	exit( $newroomid );
}
?>