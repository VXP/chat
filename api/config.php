<?php
$DB_ADDRESS = "";
$DB_LOGIN = "";
$DB_PASSWORD = "";

$chataddressfilename = $_SERVER['DOCUMENT_ROOT'] . "/api/.chataddress";
$chatloginfilename = $_SERVER['DOCUMENT_ROOT'] . "/api/.chatlogin";
$chatpassfilename = $_SERVER['DOCUMENT_ROOT'] . "/api/.chatpass";

if ( file_exists($chataddressfilename) )
{
	$DB_ADDRESS = file_get_contents($chataddressfilename);
}

if ( file_exists($chatloginfilename) )
{
	$DB_LOGIN = file_get_contents($chatloginfilename);
}

if ( file_exists($chatpassfilename) )
{
	$DB_PASSWORD = file_get_contents($chatpassfilename);
}
?>
