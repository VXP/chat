<?php

$ARGS = $_POST;
#$ARGS = $_GET;

include_once( $_SERVER['DOCUMENT_ROOT'] . "/api/config.php" );

//echo password_verify( "3", password_hash( "3", PASSWORD_DEFAULT ) );

if ( !isset( $ARGS["login"] ) || strcmp( trim($ARGS["login"]), "" ) == 0 )
{
	exit( "Что-то пошло не так." );
}
$login = $ARGS["login"];

if ( !isset( $ARGS["password"] ) || strcmp( trim($ARGS["password"]), "" ) == 0 )
{
	exit( "Что-то пошло не так." );
}
$password = $ARGS["password"];

$mode_register = false;

if ( isset( $ARGS["password2"] ) && strcmp( trim($ARGS["password2"]), "" ) != 0 )
{
	$password2 = $ARGS["password2"];
	$mode_register = true;
}

function login( $login, $password )
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

	// Получение хеша пароля для пользователя
	$query = "SELECT password FROM user WHERE login='$login' LIMIT 1";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr ));

	mysqli_free_result( $result );
	mysqli_close( $link );

	// Проверки
	if ( count($arr) <= 0 )
	{
		return false;
	}

	$hash = $arr[0]["password"];
	if ( strcmp( $hash, "" ) == 0 )
	{
		return false;
	}

	$validpass = password_verify( $password, $hash );

	// Больше не нужно
//	$query = "SELECT * FROM User WHERE login='$login' AND password='$hash'";

	return $validpass;
}

function bindsessionid()
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	global $login;
//	global $password;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

	$sessionID = session_id();

//	$query = "SELECT * FROM User WHERE login='$login' AND password='$password'";
//	$query = "UPDATE User SET sessionid='$sessionID' WHERE login='$login' AND password='$password' LIMIT 1";
	$query = "UPDATE user SET sessionid='$sessionID' WHERE login='$login' LIMIT 1";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	mysqli_free_result( $result ); // No mysqli_fetch_assoc - no gain
	mysqli_close( $link );
}

function register( $login, $password )
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

//	$query = "SELECT * FROM User WHERE login='$login' AND password='$password'";
	$query = "INSERT INTO user ( login, password ) VALUES( '$login', '$password' )";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	mysqli_free_result( $result ); // No mysqli_fetch_assoc - no gain
	mysqli_close( $link );
}

if ( $mode_register == false ) // Логин
{
	if ( login( $login, $password ) )
	{
		if ( session_start() == false )
		{
			exit( "Невозможно назначить PHPSESSID" );
		}

		// Привязываем PHPSESSID к пользователю в базе
		bindsessionid();

	//	$sessionID = session_id();
	//	exit( $sessionID );
		exit( "ID сессии назначен." );
	}
	else
	{
		exit( "Такого пользователя нет в базе. Зарегистрируйтесь" );
	}
}
else // Регистрация
{
	if ( strcmp( $password, $password2 ) != 0 )
	{
		exit( "Пароли не совпадают" );
	}

	$password = password_hash( $password, PASSWORD_DEFAULT );

	register( $login, $password );
}

?>