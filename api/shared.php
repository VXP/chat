<?php

include_once( $_SERVER['DOCUMENT_ROOT'] . "/api/config.php" );

// Shared begin
class Access
{
	const BANNED = -1;
	const USER = 0;
	const MODERATOR = 1;
	const ADMIN = 2;
	const OWNER = 3;
}

function getsessionid( &$id )
{
/* Not needed
	if ( session_start() == false )
	{
		return false;
	}

	$tmpid = session_id();
*/

	$tmpid = $_COOKIE["PHPSESSID"]; // Небольшой хак
	if ( strcmp( $tmpid, "" ) == 0 )
	{
		return false;
	}

	$id = $tmpid;
	return true;
}

// Return true if user logged in
// $retaccess - user access level, if returned true
function userAccess( &$retaccess )
{
	$retaccess = -1;

	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );

	// Shared
	$sessionID = -1;
	if ( getsessionid($sessionID) == false )
	{
		return false; // Not logged in/unlogin
	}
	// End shared

	$query = "SELECT accesslevel FROM user WHERE sessionid='$sessionID'";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );
	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr )); 

	mysqli_free_result( $result );
	mysqli_close( $link );

	// Проверки
	if ( count($arr) <= 0 )
	{
		return false; // Not logged in/unlogin
	}

	$access = $arr[0]["accesslevel"];
//	echo "access: " . $access;

//	$retaccess = $access >= $accesslevel;
	$retaccess = $access;
	return true;
}
// Shared end
?>
