<?php

date_default_timezone_set("Europe/Kyiv");
header("X-Accel-Buffering: no");
header("Content-Type: text/event-stream");
header("Cache-Control: no-cache");

include_once( $_SERVER['DOCUMENT_ROOT'] . "/api/config.php" );

$ARGS = $_REQUEST;
$roomid = $ARGS["roomid"];
$lastmessageid = $ARGS["lastmessageid"];

function getlastmessages( $roomid, $lastmessageid )
{
	global $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD;

	$link = mysqli_connect( $DB_ADDRESS, $DB_LOGIN, $DB_PASSWORD )
		or die( "Cannot connect: " . mysqli_error( $link ) );

	mysqli_select_db( $link, "chat" ) or die( "Cannot choose database" );
	
	// $messagecount = mysqli_query( $link, "SELECT COUNT(*) FROM message" ) or die( 'Cannot query: ' . mysqli_error( $link ) );
	// while(($arr0[] = mysqli_fetch_assoc( $messagecount )) || array_pop( $arr0 ));
	// echo ( $arr0[1] );

//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid AND Message.User_idUser=User.idUser LIMIT 5000 OFFSET $lastmessageid";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid LIMIT $lastmessageid, 5000";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid LIMIT 5000 OFFSET $lastmessageid";
//	$query = "SELECT * FROM Message, User WHERE room_idRoom=$roomid AND Message.User_idUser=User.idUser AND Message.idMessage>=$lastmessageid";
	$query = "SELECT idMessage, text, date, login FROM message, user WHERE room_idRoom=$roomid AND message.user_idUser=user.idUser AND message.idMessage>$lastmessageid ORDER BY message.idMessage";
	$result = mysqli_query( $link, $query ) or die( 'Cannot query: ' . mysqli_error( $link ) );

//	$arr = mysqli_fetch_array( $result, mysqli_ASSOC ); // Only on loops?
	while(($arr[] = mysqli_fetch_assoc( $result )) || array_pop( $arr ));

	mysqli_free_result( $result );
	mysqli_close( $link );

	return $arr;
}

// $counter = rand(1, 10);
//$lastTimeChecked = 0;
while (true)
{
//	if ( time() < $lastTimeChecked + 1000 )
//		continue;
//	$lastTimeChecked = time();
	sleep(1);
	
	

//	echo "event: ping\n";
//	$curDate = date(DATE_ISO8601);
////	echo 'data: {"time": "' . $curDate . '"}';
//	echo 'data: {"roomid": "' . $roomid . '", "lastmessageid": "' . $lastmessageid . '"}';
//	echo "\n\n";

	// Send a simple message at random intervals.

	// $counter--;

	// if (!$counter)
	// {
		// echo 'data: This is a message at time ' . $curDate . "\n\n";
		// $counter = rand(1, 10);
	// }
	$arr = getlastmessages( $roomid, $lastmessageid );
	if ( count( $arr ) <= 0 )
		continue;
	echo "event: newmessage\n";
	echo "data: " . json_encode( $arr, JSON_UNESCAPED_UNICODE );
	echo "\n\n";

	if (ob_get_contents())
	{
		ob_end_flush();
	}
	flush();

	// Break the loop if the client aborted the connection (closed the page)
	if (connection_aborted())
		break;

	
}

?>
