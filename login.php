<?php
// if ( isset( $_COOKIE["PHPSESSID"] ) )
// {
	// header( "Location: index.php" );
// }

include_once( $_SERVER['DOCUMENT_ROOT'] . "/api/shared.php" );

$dummy = -1;
if ( userAccess( $dummy ) )
{
	header( "Location: index.php" );
}
?>
<html>
<head>
<meta name="viewport" content="width=400">
<title>Логин</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script>
function getXmlHttp(){var xmlhttp;
try{xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){
try{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}catch(E){xmlhttp=false;}}
if(!xmlhttp && typeof XMLHttpRequest!='undefined'){xmlhttp=new XMLHttpRequest();}return xmlhttp;}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function login()
{
	var xmlhttp = getXmlHttp();

	var login = document.getElementById( "login" ).value;
	var password = document.getElementById( "password" ).value;

	xmlhttp.open( 'POST', 'api/auth.php', true );
	xmlhttp.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
	xmlhttp.send( "login=" + encodeURIComponent( login ) + "&password=" + encodeURIComponent( password ) );
	xmlhttp.onreadystatechange = function()
	{
		if ( xmlhttp.readyState == 4 )
		{
			if ( xmlhttp.status == 200 )
			{
				console.log( xmlhttp.responseText );
				console.log( document.cookie );
				var sessionid = getCookie( "PHPSESSID" );
				if ( sessionid )
				{
					console.log( "Login successful" );
					window.location = ".";
				}
				else
				{
					console.log( "Login unsuccessful" );
					showError( xmlhttp.responseText )
				}
			}
		}
	};

}

function showError( message )
{
	if ( message === undefined || message === "" )
	{
		document.getElementById( "error" ).style.visibility = "hidden";
		return;
	}

	document.getElementById( "error" ).style.visibility = "visible";
	document.getElementById( "error" ).innerHTML = message;
}
</script>
</head>
<body>
	Войдите в чат с помощью логина и пароля:<br>
	Если Вы ещё не зарегистрированы - нажмите <a href="register.php">Регистрация</a><br>
	<br>
	<input type="text" id="login" name="login" value="" placeholder="Логин"><br>
	<input type="password"id="password" name="password" value="" placeholder="Пароль" onkeydown="if ( event.keyCode == 13 ) login();"><br>
	<button onclick="login();">Войти</button>
	<div id="error"></div>
</body>
</html>